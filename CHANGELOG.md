# Changelog

## v3.0.0

* huge refactor dut to WoT 1.18.0 changes

## v2.7.1

* xfw_hooks: try to fix hang on Nvidia graphic cards

## v2.7.0

* libpython/data: add WoT 1.17.1 support
* xfw_hooks: reintroduce, replaces minhook

## v2.6.3

* 3rdparty: update pybind11 to 2.9.2
* 3rdparty: update xxhash to 2022.05.24
* libpython/data: add WoT 1.17.0 support

## v2.6.2

* libpython/data: add WoT 1.16.1 support

## v2.6.1

* libpython/data: add WoT 1.16.0 support

## v2.6.0

* build: refactor

## v2.5.0

* 3rdparty: update pybind11 to 2.8.1
* 3rdparty: update xxhash to 2021.10.28
* build: refactor (still in WIP)
* build: change toolchain to v143
* libpython: fix additional modules importing for .pyd files
* libpython/data: add WoT 1.15.0 CT  support

## v2.4.2

* libpython/data: add WoT 1.14.1 CT hybrid support

## v2.4.1

* libpython/data: add WoT 1.14.1 CT support

## v2.4.0

* build: add ability to build only part of project
* libpython/data: some additional functions and structures for _socket for 32-bit client
* python/socket: add and enable for i686
* python/ssl: enable for i686
* python/httplib: enable for i686
* python/urllib: enable for i686
* python/urllib2: enable for i686
* wot_python: fix initialization on Windows 11

## v 2.3.0

* depends/openssl: update to 1.1.1l
* depends/pybind11: update to 2.7.1
* depends/xxhash: update to latest dev
* libpython/data: some additional functions and structures for _socket and _ssl modules
* python/socket: add and enable for AMD64
* python/ssl: enable for AMD64
* python/httplib: enable for AMD64
* python/urllib: enable for AMD64
* python/urllib2: enable for AMD64

## v 2.2.0

* libpython/data: add support for WoT 1.14

## v 2.1.9

* libpython/data: add support for WoT 1.13

## v 2.1.8

* wotexport: switch from .dll to .lib

## v 2.1.7

* build: add ability to upload symbols
* dependencies: update xxHash and pybind11
* libpython/data: add support for WoT 1.11.1

## v 2.1.6

* d3dhook: remove it (will be part of other project)
* dependencies: update xxHash and pybind11

## v 2.1.5

* d3dhook: fix crash and reenable

## v 2.1.4

* libpython/data: add support for WoT 1.10.1 i386 hybrid

## v 2.1.3

* d3dhook: disable because of random crashes

## v 2.1.2

* libpython/data: add support for WoT 1.10.1

## v 2.1.1

* wotexport: add support for WoT 1.10.1

## v 2.1.0

* d3dhook: fix offset finding and improve API

## v 2.0.9

* libpython/data: add support for WoT 1.10 i386 hybrid

## v 2.0.8

* libpython/data: add support for WoT 1.10 AMD64 hybrid

## v 2.0.7

* wot_python: pop native modules location from sys.path on libpython load error
* meta: disable 32bit support (temporarily)

## v 2.0.6

* libpython/data: add support for WoT 1.10 AMD64
* libpython/data: add PyExc_FutureWarning
* libpython: partial migration to C
* libpython: autogen cleanup

## v 2.0.5

* libpython/data: add some new signatures
* libpython: partial migration to C

## v 2.0.4

* libpython/data: add support for WoT 1.9.1 hybrid
* libpython: partial migrate to C
* libpython: replace SHA1 with XXhash3

## v 2.0.3

* libpython/data: add support for WoT 1.9.1
* libpython/data: add PyNumber for AMD64
* wot_python: add `unpack_load_native` function

## v 2.0.2

* build: refactor build script and devel layout
* libpython: cleanup ASM files

## v 2.0.1

* libpython/data: add support for WoT 1.9.0.3

## v 2.0.0

* 3rdparty/pybind11: update to 2.5.0
* 3rdparty/sqlite: update to 3.31.1
* libpython/data: add support for WoT 1.9.0.2
* libpython/data: massive update for AMD64
* libpython: delete XfwNative API
* python/cpython: update to 2.7.18
* python/sqlite: update to 2.7.17

## v 1.6.2

* libpython/data: add support for WoT 1.9.0.0 hybrid client

## v 1.6.1

* libpython/data: add support for WoT 1.9.0.0

## v 1.6.0

* build: added support for code signing
* libpython/data: changed offsets file format

## v 1.5.9

* libpython/data: add support for WoT 1.7.1.2

## v 1.5.8

* libpython: fix PyMem_Malloc and PyMem_Realloc
* python: fix exception message for non-latin languages

## v 1.5.7

* libpython: fix Py_GetPlatform, PyErr_Fetch, PyString_ConcatAndDel
* libpython/updater: initial 64-bit support

## v 1.5.6
* audiokinetic: provide only headers
* wotexport: first version

## v 1.5.5
* python: fixed compatibility with non-latin characters in path

## v 1.5.4
* build: disable FrameHandler4
* libpython: switch from PPL to Yuki Koyama's parallel util

## v 1.5.3
* libpython: switch from OpenMP to MS PPL for concurrency

## v 1.5.2
* libpython: add additional 64-bit signatures

## v 1.5.1
* libpython: initial 64-bit support

## v. 1.5.0
* libpython: implemented parallel signature search via OpenMP
* libpython: preparation for 64-bit

## v. 1.4.9
* python: fix `unpack_native` behaviour
* python: add `xfw_is_module_loaded` function
* python: fix false `__native_available` flag value on error

## v. 1.4.8
* adopt to WoT 1.6.1.0

## v. 1.4.7
* updated meta

## v. 1.4.6
* move repository to GitLab

## v. 1.4.5
* libpython: finished PyCapsule API
* libpython: added PyUnicodeUCS2_DecodeUTF[16|32]{Stateful}
* pybind11: added pybind11 v2.3.0
* sqlite: fixed module initialization

## v. 1.4.4
* audiokinetic: fixed module initialization
* ida: removed dependency from simplejson
* libpython: fixed cache file deserialization
* libpython: add offsets for WoT 1.6.0.365
* libpython: fixed crash in `PyUnicodeUCS_FromFormat`
* libpython: updated `object.h` and `intobject.h` headers to CPython 2.7.16
* sqlite: add `sqlite3` module backport from CPython 2.7.16

## v. 1.4.3

* libpython: added `PyUnicodeUCS_FromFormat` and `PyUnicodeUCS_FromFormatV`
* libpython: fixed structures array size
* libaudiokinetic: added SoundEngine component registration functions
* build: fixed compilation for CMake 3.14 and MSVS 2019

## v. 1.4.2

* libpython: fixed compatibility with WoT 1.4.0.227
* tool_ida: added ability to import types to IDA for non-WoT binaries

## v. 1.4.1

* libpython: fixed compatibility with WoT 1.3.0.126
* libpython: improved debug output


## v. 1.4.0

* libpython: fixed compatibility with WoT 1.2.0.58
* libpython: new JSON file format
* libpython: added support for multiple signatures for one structure
* libpython: fixed multiple matching validation


## v. 1.3.1

* xfw_d3dhook: added ability to hook IDXGISwapChain:ResizeTarget
* xfw_d3dhook: rename D3D11 to DXGI

## v. 1.3.0

* xfw_d3dhook: added library for D3D9/DXGI hooking
* xfw_package: added `features_provide` section
* libpython: updated offsets
* libpython: added `extern "C"` in `PythonWoT.h` header
* libpython: implemented PyCode::Type
* libpython: implemented PyFrozenSet
* libpython: implemented PyFunction
* libpython: implemented PyNumber
* libpython: implemented PyInstance
* libpython: implemented PyObject
* libpython: implemented PySet
* libpython: implemented PyUnicodeUCS::Compare

## v. 1.2.0

* implemented libpython signatures cache
* implemented structures runtime validation

## v. 1.1.1

* World of Tanks 1.0.1 Micropatch 1

## v. 1.1.0

* World of Tanks 1.0.1 Common Test 2
* Unpack native modules to game client directory instead of user temporary directory due to World of Tanks bug

## v. 1.0.9

* World of Tanks 1.0.0 micropatch 1

## v. 1.0.8

* World of Tanks 1.0.0 Common Test 6
* Improve debug output
* Improve runtime signature matching

## v. 1.0.7

* fix null pointer derefencing on dynamic minhook loading

## v. 1.0.6

* compatible with WoT 9.22 CT

## v. 1.0.5

* audiokinetic: added `AK:Comm` namespace
* libpython: fix some data structures
* libpython: implement some more types structures

##  v. 1.0.4

* export some helper functions

## v. 1.0.3

* provide signed binaries
* add some new signatures

## v. 1.0.2

* compatible with WoT 9.21.0
* update xfw_packages.json to latest specification


## v. 1.0.1

* change **.wotmod** and **.zip** layout


## v. 1.0.0

* first public version
* compatible with WoT 9.20.1
